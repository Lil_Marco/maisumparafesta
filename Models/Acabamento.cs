using System;

namespace SiCProject.Models
{
    public class Acabamento
    {
        public int ID {get; set;}

        public string Nome {get; set;}

        public float Incremento {get;set;}

        public int MaterialId {get; set;}

    }
}