﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiCProject.Models
{
    public class CatalogoProduto
    {

        public int ID { get; set; }

        public int ProdutoID { get; set; }

        public int CatalogoID { get; set; }
    }
}
