using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.DTOs;
using SiCProject.Repositories;

namespace SiCProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ColecaoController : ControllerBase
    {
        private readonly SiCContext _context;
        private ColecaoRepository _repository;
        public ColecaoController(SiCContext context)
        {
            _context = context;
            _repository = new ColecaoRepository(_context);
        }

        // GET: api/Colecao
        [HttpGet]
        public async Task<IActionResult> GetColecoes()
        {
            List<Colecao> lista = _repository.GetAll();

            List<ColecaoDTO> dtos = new List<ColecaoDTO>();

            foreach(Colecao c in lista){
                dtos.Add(_repository.getInfoColecaoDTO(c));
            }

            return Ok(dtos);
        }

        // GET: api/Colecao/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetColecaoById([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Colecao colecao = _repository.GetByID(id);

            if (colecao == null)
            {
                return NotFound();
            }

            return Ok(_repository.getInfoColecaoDTO(colecao));
        }

        // PUT: api/Colecao/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutColecao([FromRoute] int id, [FromBody] Colecao colecao)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != colecao.ID)
            {
                return BadRequest();
            }

            if(_repository.Put(colecao)){
                 return NoContent();
            }else{
                return NotFound();
            }
        }

        // POST: api/Colecao
        [HttpPost]
        public async Task<IActionResult> PostColecao([FromBody] Colecao colecao)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Colecao col = _repository.Post(colecao);

            return Ok(_repository.getInfoColecaoDTO(colecao));
        }

        // DELETE: api/Colecao/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteColecao([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if(_repository.Delete(id)){
                return NoContent();
            }else{
                return NotFound();
            }
        }

        private bool ColecaoExists(int id)
        {
            return _context.Colecao.Any(e => e.ID == id);
        }
    }
}