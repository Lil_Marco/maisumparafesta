using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;

namespace SiCProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MedidaController : ControllerBase
    {
        private readonly SiCContext _context;

        public MedidaController(SiCContext context)
        {
            _context = context;
        }

        // GET: api/Medida
        [HttpGet]
        public IEnumerable<Medida> GetMedidas()
        {
            return _context.Medidas;
        }

        // GET: api/Medida/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetMedida([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var medida = await _context.Medidas.FindAsync(id);

            if (medida == null)
            {
                return NotFound();
            }

            return Ok(medida);
        }

        // PUT: api/Medida/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMedida([FromRoute] int id, [FromBody] Medida medida)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != medida.ID)
            {
                return BadRequest();
            }

            _context.Entry(medida).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MedidaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Medida
        [HttpPost]
        public async Task<IActionResult> PostMedida([FromBody] Medida medida)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Medidas.Add(medida);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMedida", new { id = medida.ID }, medida);
        }

        // DELETE: api/Medida/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMedida([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var medida = await _context.Medidas.FindAsync(id);
            if (medida == null)
            {
                return NotFound();
            }

            _context.Medidas.Remove(medida);
            await _context.SaveChangesAsync();

            return Ok(medida);
        }

        private bool MedidaExists(int id)
        {
            return _context.Medidas.Any(e => e.ID == id);
        }
    }
}