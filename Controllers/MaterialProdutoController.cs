using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;

namespace SiCProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MaterialProdutoController : ControllerBase
    {
        private readonly SiCContext _context;

        public MaterialProdutoController(SiCContext context)
        {
            _context = context;
        }

        // GET: api/MaterialProduto
        [HttpGet]
        public IEnumerable<MaterialProduto> GetMateriaisProduto()
        {
            return _context.MateriaisProduto;
        }

        // GET: api/MaterialProduto/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetMaterialProduto([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var materialProduto = await _context.MateriaisProduto.FindAsync(id);

            if (materialProduto == null)
            {
                return NotFound();
            }

            return Ok(materialProduto);
        }

        // PUT: api/MaterialProduto/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMaterialProduto([FromRoute] int id, [FromBody] MaterialProduto materialProduto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != materialProduto.ID)
            {
                return BadRequest();
            }

            _context.Entry(materialProduto).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MaterialProdutoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MaterialProduto
        [HttpPost]
        public async Task<IActionResult> PostMaterialProduto([FromBody] MaterialProduto materialProduto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.MateriaisProduto.Add(materialProduto);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMaterialProduto", new { id = materialProduto.ID }, materialProduto);
        }

        // DELETE: api/MaterialProduto/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMaterialProduto([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var materialProduto = await _context.MateriaisProduto.FindAsync(id);
            if (materialProduto == null)
            {
                return NotFound();
            }

            _context.MateriaisProduto.Remove(materialProduto);
            await _context.SaveChangesAsync();

            return Ok(materialProduto);
        }

        private bool MaterialProdutoExists(int id)
        {
            return _context.MateriaisProduto.Any(e => e.ID == id);
        }
    }
}