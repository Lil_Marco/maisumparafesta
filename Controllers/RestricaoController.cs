using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.Models.Restricoes;

namespace SiCProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RestricaoController : ControllerBase
    {
        private readonly SiCContext _context;

        public RestricaoController(SiCContext context)
        {
            _context = context;
        }

        // GET: api/Restricao
        [HttpGet]
        public IEnumerable<Restricao> GetRestricao()
        {
            return _context.Restricoes;
        }

        // GET: api/Restricao/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetRestricao([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var restricao = await _context.Restricoes.FindAsync(id);

            if (restricao == null)
            {
                return NotFound();
            }

            return Ok(restricao);
        }

        // PUT: api/Restricao/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRestricao([FromRoute] int id, [FromBody] Restricao restricao)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != restricao.ID)
            {
                return BadRequest();
            }

            _context.Entry(restricao).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RestricaoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Restricao
        [HttpPost]
        public async Task<IActionResult> PostRestricao([FromBody] Restricao restricao)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Restricoes.Add(restricao);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetRestricao", new { id = restricao.ID }, restricao);
        }

        // DELETE: api/Restricao/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRestricao([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var restricao = await _context.Restricoes.FindAsync(id);
            if (restricao == null)
            {
                return NotFound();
            }

            _context.Restricoes.Remove(restricao);
            await _context.SaveChangesAsync();

            return Ok(restricao);
        }

        private bool RestricaoExists(int id)
        {
            return _context.Restricoes.Any(e => e.ID == id);
        }
    }
}