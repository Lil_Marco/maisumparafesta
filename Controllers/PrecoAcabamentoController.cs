using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.Repositories;
using SiCProject.DTOs;

namespace SiCProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PrecoAcabamentoController : ControllerBase
    {
        private readonly SiCContext _context;
        private PrecoAcabamentoRepository _repository;

        public PrecoAcabamentoController(SiCContext context)
        {
            _context = context;
            _repository = new PrecoAcabamentoRepository(_context);
        }

        // GET: api/PrecoAcabamento
        [HttpGet]
        public async Task<IActionResult> GetPrecoAcabamentos()
        {
            List<PrecoAcabamento> lista = _repository.GetAll();
            List<PrecoAcabamentoDTO> dtos = new List<PrecoAcabamentoDTO>();

            foreach (PrecoAcabamento preco in lista)
            {
                dtos.Add(_repository.getInfoPrecoAcabamentoDTO(preco));
            }

            return Ok(dtos);
        }

        // GET: api/PrecoAcabamento/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPrecoAcabamento([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            PrecoAcabamento precoAcabamento = _repository.GetByID(id);

            if (precoAcabamento == null)
            {
                return NotFound();
            }

            return Ok(_repository.getInfoPrecoAcabamentoDTO(precoAcabamento));
        }

        // PUT: api/PrecoAcabamento/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPrecoAcabamento([FromRoute] int id, [FromBody] PrecoAcabamento precoAcabamento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != precoAcabamento.ID)
            {
                return BadRequest();
            }

            if (_repository.Put(precoAcabamento))
            {
                return NoContent();
            }
            else
            {
                return NotFound();
            }
        }

        // POST: api/PrecoAcabamento
        [HttpPost]
        public async Task<IActionResult> PostPrecoAcabamento([FromBody] PrecoAcabamento precoAcabamento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            PrecoAcabamento preco = _repository.Post(precoAcabamento);


            return Ok(_repository.getInfoPrecoAcabamentoDTO(preco));
        }

        // DELETE: api/PrecoAcabamento/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePrecoAcabamento([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (_repository.Delete(id))
            {
                return NoContent();
            }
            else
            {
                return NotFound();
            }
        }

        private bool PrecoAcabamentoExists(int id)
        {
            return _context.PrecoAcabamentos.Any(e => e.ID == id);
        }
    }
}