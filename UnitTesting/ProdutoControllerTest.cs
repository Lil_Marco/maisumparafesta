/*using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.Controllers;
using Xunit;

namespace SiCProject.UnitTesting
{
    public class ProdutoControllerTest
    {
        private SiCContext InitDBSet(String DbName){
            SiCContext _context;

            var option = new DbContextOptionsBuilder<SiCContext>()
                .UseInMemoryDatabase(DbName)
                .Options;

            _context = new SiCContext(option);
            _context.Database.EnsureCreated();
            _context.Database.EnsureDeleted();

            return _context;
        }

        #pragma warning disable 1998

        private void DB(SiCContext _context){

            Produto p1 = new Produto{
                Nome = "prod1",
                Preço = 2,
                Categoria=null,
                Material=null,
                Dimensao=null
            };

            Produto p2 = new Produto{
                Nome = "prod2",
                Preço = 2,
                Categoria=null,
                Material=null,
                Dimensao=null
            };

            _context.Produtos.Add(p1);
            _context.Produtos.Add(p2);

            _context.SaveChanges();
        }

        [Fact]
        public void Test_GetAll(){
            using(var _context = InitDBSet("Test_GetAll_C")){

                DB(_context);
                var _mController = new ProdutoController(_context);
                long tamanho_esperado = 2;

                int resultado = 0;
                IEnumerable<Produto> lol = _context.Produtos;
                using(IEnumerator<Produto> CharEnumerator = lol.GetEnumerator()){
                    while(CharEnumerator.MoveNext())
                        resultado++;
                }

                Assert.Equal(tamanho_esperado, resultado);
            }
        }

    [Fact]
        public async void Test_PutProduto()
        {
            using (var _context = InitDBSet("Test_PutProduto_M"))
            {
                var _mController = new ProdutoController(_context);
                Assert.Equal(0, await _context.Produtos.CountAsync());

                Produto m = new Produto
                {
                    Nome = "prod1_test"
                };

                await _context.Produtos.AddAsync(m);
                await _context.SaveChangesAsync();

                Assert.Equal(1, await _context.Produtos.CountAsync());

                int mid = 1;
                m.Nome = "Test";

                var acab = await _mController.PutProduto(mid, m);

            }
        }
        [Fact]
        public async void Test_PostProduto()
        {
            using (var _context = InitDBSet("Test_PostProduto"))
            {
                DB(_context);
                var _mController = new ProdutoController(_context);

                Produto m = new Produto();
            }
        }

        [Fact]
        public async void Test_DeleteProduto()
        {
            using (var _context = InitDBSet("Test_DeleteProduto_M"))
            {
                Produto m = new Produto
                {
                    Nome = "prod2",
                    Preço = 2,
                    Categoria=null,
                    Material=null,
                    Dimensao=null
                };

                _context.Produtos.Add(m);

                _context.SaveChanges();

                var _mController = new ProdutoController(_context);

                var p = await _mController.DeleteProduto(m.ID);
                Assert.IsType<NoContentResult>(p);
            }
        }

        [Fact]
        public async void Test_GetProduto()
        {
            using (var _context = InitDBSet("Test_GetProduto"))
            {
                DB(_context);
                var _mController = new ProdutoController(_context);

                Produto m = await _context.Produtos.FirstAsync();

            }
        }
    }
} */