using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.Models.Restricoes;
using SiCProject.Services;
using Xunit;

namespace SiCProject.UnitTesting
{
    public class ServiçoTest
    {
        private SiCContext InitDBSet(String DbName){
            SiCContext _context;

            var option = new DbContextOptionsBuilder<SiCContext>()
                .UseInMemoryDatabase(DbName)
                .Options;

            _context = new SiCContext(option);
            _context.Database.EnsureCreated();
            _context.Database.EnsureDeleted();

            return _context;
        }

        private void DB(SiCContext _context){

            _context.SaveChanges();
        }

        [Fact]
        public void Test_restricaoCaber(){
            using(var _context = InitDBSet("Test_restricaoCaber")){

                DB(_context);

                double alturaPai = 5;
                double alturaFilho = 2;
                double larguraPai = 10;
                double larguraFilho = 6;
                double profundidadePai = 15;
                double profundidadeFilho = 8;
                bool resultado = true;

                bool resultado2 = Serviço.restricaoCaber(alturaPai, alturaFilho, larguraPai,
                larguraFilho, profundidadePai, profundidadeFilho);

                Assert.Equal(resultado, resultado2);
            }
        }

        [Fact]
        public void Test_getVolume(){
            using(var _context = InitDBSet("Test_getVolume")){

                DB(_context);

                double altura = 2;
                double profundidade = 2;
                double largura = 2;
                double resultado = 8;

                double resultado2 = Serviço.getVolume(altura, largura, profundidade);

                Assert.Equal(resultado, resultado2);
            }
        }

        [Fact]
        public void Test_restricaoOcupacao(){
            using(var _context = InitDBSet("Test_restricaoOcupacao")){

                DB(_context);

                var restricao = new RestricaoOcupacao();

                restricao.limMin = 10;
                restricao.limMax = 50;

                double VolumePai = 750;
                double VolumeFilho = 96;

                bool resultado = true;

                bool resultado2 = Serviço.restricaoOcupacao(restricao, VolumePai, VolumeFilho);

                Assert.Equal(resultado, resultado2);
            }
        }

        [Fact]
        public void Test_restricaoMaterial(){
            using(var _context = InitDBSet("Test_restricaoMaterial")){

                DB(_context);

                var restricao = new RestricaoMaterial();

                restricao.material = "MDF";

                String materialPai = "madeira";
                String materialFilho = "ferro";
                String materialFilho2 = "madeira";

                bool resultado = false;
                bool resultado2 = true;

                bool resultado3 = Serviço.restricaoMaterial(restricao, materialPai, materialFilho);
                bool resultado4 = Serviço.restricaoMaterial(restricao, materialPai, materialFilho2);

                Assert.Equal(resultado, resultado3);
                Assert.Equal(resultado2, resultado4);
            }
        }

    }
}