using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.DTOs;


namespace SiCProject.Repositories{
    public class MaterialRepository : MaterialRepositoryInterface{

        private readonly SiCContext _context;

        public MaterialRepository(SiCContext context)
        {
            _context = context;
        }

         public MaterialDTO getInfoMaterialDTO(Material m){
            
            List<String> acabs = new List<String>();
            foreach(Acabamento acabamento in _context.Acabamentos){
                if(acabamento.MaterialId == m.ID){
                    acabs.Add(_context.Acabamentos.Find(acabamento.ID).Nome);
                } 
            }
            return new MaterialDTO(m.ID, m.Nome, acabs, m.Preco);
        }

        public List<Material> GetAll()
        {
            List<Material> mats = new List<Material>();       
            foreach(Material material in _context.Materiais){
                mats.Add(material);
            }
            return mats;
        }

        public Material GetByID(int id)
        {
            Material material = _context.Materiais.Find(id);

            return material;
        }

        public  List<Material> GetByNome(string nome)
        {
            IQueryable<Material> material = _context.Materiais.Where(m=>m.Nome.Equals(nome));//.FirstOrDefault<Produto>();
            List<Material> lista = material.ToList();

            return lista;
        }

        public Material Post(Material obj)
        {
            List<Material> lista = this.GetAll();

            bool flag = false;
            foreach (Material m in lista)
            {
                if (m.Nome == obj.Nome)
                {
                    flag = true;
                }
            }
            if (flag == false)
            {
                _context.Materiais.Add(obj);
                _context.SaveChangesAsync();

                return obj;
            }
            return null;
        }

        public bool Put(Material obj)
        {
            _context.Entry(obj).State = EntityState.Modified;

            try{
                _context.SaveChangesAsync();
            }catch(DbUpdateConcurrencyException){
                return false;
            }
            return true;
        }

        public bool Delete(int id)
        {
            var material = _context.Materiais.Find(id);

            if(material == null){
                return false;
            }
            
            _context.Materiais.Remove(material);
            _context.SaveChangesAsync();

            return true;
        }

        private bool MaterialExists(int id)
        {
            return _context.Materiais.Any(e => e.ID == id);
        }
    }
}