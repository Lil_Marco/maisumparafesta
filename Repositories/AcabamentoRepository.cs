using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.DTOs;


namespace SiCProject.Repositories
{
    public class AcabamentoRepository : AcabamentoRepositoryInterface
    {

        private readonly SiCContext _context;

        public AcabamentoRepository(SiCContext context)
        {
            _context = context;
        }

        public AcabamentoDTO getInfoAcabamentoDTO(Acabamento a)
        {
            return new AcabamentoDTO(a.ID, a.Nome, a.Incremento, a.MaterialId);
        }

        public List<Acabamento> GetAll()
        {
            List<Acabamento> acab = new List<Acabamento>();
            foreach (Acabamento acabamento in _context.Acabamentos)
            {
                acab.Add(acabamento);
            }
            return acab;
        }

        public Acabamento GetByID(int id)
        {
            Acabamento acabamento = _context.Acabamentos.Find(id);

            return acabamento;
        }

        public List<Acabamento> GetByNome(string nome)
        {
            IQueryable<Acabamento> acabamento = _context.Acabamentos.Where(a => a.Nome.Equals(nome));
            List<Acabamento> lista = acabamento.ToList();

            return lista;
        }

        public Acabamento Post(Acabamento obj)
        {
            List<Acabamento> lista = this.GetAll();

            bool flag = false;
            foreach (Acabamento m in lista)
            {
                if (m.Nome == obj.Nome)
                {
                    flag = true;
                }
            }
            if (flag == false)
            {
                _context.Acabamentos.Add(obj);
                _context.SaveChangesAsync();

                return obj;
            }
            return null;
        }

        public bool Put(Acabamento obj)
        {
            _context.Entry(obj).State = EntityState.Modified;

            try
            {
                _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return false;
            }
            return true;
        }

        public bool Delete(int id)
        {
            var acabamento = _context.Acabamentos.Find(id);

            if (acabamento == null)
            {
                return false;
            }

            _context.Acabamentos.Remove(acabamento);
            _context.SaveChangesAsync();

            return true;
        }

        private bool AcabamentoExists(int id)
        {
            return _context.Acabamentos.Any(e => e.ID == id);
        }
    }
}