using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.DTOs;


namespace SiCProject.Repositories{
    public class CategoriaRepository : CategoriaRepositoryInterface{

        private readonly SiCContext _context;

        public CategoriaRepository(SiCContext context)
        {
            _context = context;
        }

        public CategoriaDTO getInfoCategoriaDTO(Categoria c){
            
            List<int> subCategorias = new List<int>();
            foreach(ComposicaoCategoria compCategoria in _context.ComposicoesCategorias){
                if(compCategoria.CategoriaPaiID == c.ID){
                    subCategorias.Add(compCategoria.CategoriaFilhoID);
                } 
            }
            return new CategoriaDTO(c.ID, c.nome, subCategorias);
        }

        public List<Categoria> GetAll()
        {
            List<Categoria> cats = new List<Categoria>();       
            foreach(Categoria categoria in _context.Categorias){
                cats.Add(categoria);
            }
            return cats;
        }

        public Categoria GetByID(int id)
        {
            Categoria categoria = _context.Categorias.Find(id);

            return categoria;
        }

        public Categoria Post(Categoria obj)
        {
            _context.Categorias.Add(obj);

            _context.SaveChangesAsync();

            return obj;
        }

        public bool Put(Categoria obj)
        {
            _context.Entry(obj).State = EntityState.Modified;

            try{
                _context.SaveChangesAsync();
            }catch(DbUpdateConcurrencyException){
                return false;
            }
            return true;
        }

        public bool Delete(int id)
        {
            var categoria = _context.Categorias.Find(id);

            if(categoria == null){
                return false;
            }
            
            _context.Categorias.Remove(categoria);
            _context.SaveChangesAsync();

            return true;
        }

        private bool CategoriaExists(int id)
        {
            return _context.Categorias.Any(e => e.ID == id);
        }
    }
}