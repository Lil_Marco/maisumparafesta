using System;
using SiCProject.Models;
using System.Collections.Generic;

namespace SiCProject.DTOs
{
    public class ProdutoDTO
    {
        public int ID { get; set; }
        public string Nome { get; set; }
        public float Preço { get; set; }
        public int Categoria { get; set; }
        public List<String> Materiais { get; set; }
        public List<String> Cores { get; set; }
        public List<double> Altura { get; set; }
        public List<double> Largura { get; set; }
        public List<double> Profundidade { get; set; }
        public List<double> RestricaoOcup { get; set; }
        public List<String> RestricoesMaterial { get; set; }
        public List<int> Filhos { get; set; }

        public ProdutoDTO(int id, string nomeprod, float preço, int cat, List<String> mats, List<String> cor,
        List<double> altura, List<double> largura, List<double> profundidade, List<double> ocup,
        List<String> restMat, List<int> filhos)
        {
            this.ID = id;
            this.Nome = nomeprod;
            this.Preço = preço;
            this.Categoria = cat;
            this.Materiais = mats;
            this.Altura = altura;
            this.Largura = largura;
            this.Profundidade = profundidade;
            this.RestricaoOcup = ocup;
            this.RestricoesMaterial = restMat;
            this.Filhos = filhos;
            this.Cores = cor;
        }

    }
}