using System;
using SiCProject.Models;
using System.Collections.Generic;

namespace SiCProject.DTOs
{
    public class DimensaoDTO
    {
        public int ID {get; set;}
        public List<double> altura {get; set;}
        public List<double> largura {get; set;}
        public List<double> profundidade {get; set;}

        public DimensaoDTO(int id, List<double> alt, List<double> larg, List<double> prof){
            this.ID = id;
            this.altura = alt;
            this.largura = larg;
            this.profundidade = prof;
        }

    }
}