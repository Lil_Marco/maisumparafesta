﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiCProject.DTOs
{
    public class CatalogoDTO
    {

        public CatalogoDTO(int iD, string nome, List<ProdutoDTO> lProdutos)
        {
            this.ID = iD;
            this.Nome = nome;
            this.lProdutos = lProdutos;
        }

        public int ID { get; set; }
        public string Nome { get; set; }

        public List<ProdutoDTO> lProdutos { get; set; }
    }

}
