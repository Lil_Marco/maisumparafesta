using System;
using SiCProject.Models;
using System.Collections.Generic;

namespace SiCProject.DTOs
{
    public class PrecoMaterialDTO
    {
        public int ID { get; set; }
        public float preco { get; set; }
        public DateTime data { get; set; }
        public int MaterialId { get; set; }

        public PrecoMaterialDTO(int id, float preco, DateTime data, int materialId)
        {
            this.ID = id;
            this.preco = preco;
            this.data = data;
            this.MaterialId = materialId;
        }

    }
}