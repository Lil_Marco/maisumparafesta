using System;

public class NaoCabeException : Exception
{
    public NaoCabeException() : base() { }
    public NaoCabeException(string message) : base(message) { }
    public NaoCabeException(string message, System.Exception inner) : base(message, inner) { }

    // A constructor is needed for serialization when an
    // exception propagates from a remoting server to the client. 
    protected NaoCabeException(System.Runtime.Serialization.SerializationInfo info,
        System.Runtime.Serialization.StreamingContext context) { }
}